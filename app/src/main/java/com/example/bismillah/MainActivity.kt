package com.example.bismillah

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadInfo(InfoFragment())
        info.setOnClickListener {
            loadInfo(InfoFragment())
        }

        cegah.setOnClickListener {
         loadCegah(CegahFragment())

}
        profile.setOnClickListener {
            loadProfile(ProfileFragment())

        }
    }

    private fun loadInfo(frag1: InfoFragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment, frag1)
        ft.commit()
    }
    private fun loadCegah(frag2: CegahFragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment, frag2)
        ft.commit()
    }
    private fun loadProfile(frag3: ProfileFragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment, frag3)
        ft.commit()
    }
}
